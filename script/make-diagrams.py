#!/usr/bin/env python3

import regression
import pandas as pd
import numpy as np

# all quantities in cm
displacement_error = 0.1
separation_error = 0.2

paths = ['../data/survey_1.csv', '../data/survey_2.csv',
         '../data/survey_3.csv']

separations = [45.8+5.5, 15.5+5.5, 20.0+5.5]
names = ['survey_1', 'survey_2', 'survey_3']

for i, path in enumerate(paths):
    dataset = pd.read_csv(path, comment='#')
    print(dataset.keys())
    steps = dataset['servo_step']
    angles = np.arctan(dataset['displacement']/separations[i])
    angles_error = 1/(separations[i]**2 + dataset['displacement']**2)*\
                   np.sqrt((separations[i]*displacement_error)**2
                           + (dataset['displacement']*separation_error)**2)

    # conversion to degrees
    angles = np.rad2deg(angles)
    angles_error = np.rad2deg(angles_error)

    fig = regression.LinearRegression(steps, angles, angles_error,
                                      tags=['Paso', 'Ángulo\,/\,°'])
    fig.scatter_plot(names[i])
