#!/usr/bin/env python3

import numpy as np
from scipy import optimize
from scipy import stats
import matplotlib.pyplot as plt
import tikzplotlib

def stdev(xdata, ydata, slope, n):
    sum_differences_squared = sum((ydata - slope*xdata)**2)
    stdev = np.sqrt(sum_differences_squared/(n-2))
    return stdev

def stdev_mean(xdata, ydata, slope, n):
    ''' This is not the measure of the standard error '''
    st_dev = stdev(xdata, ydata, slope, n)
    stdev_mean = st_dev/np.sqrt(n)
    return stdev_mean

def confidence_interval(uncertainty, deg_freedom, mean, std_error):
    t_value = stats.t.interval(1-uncertainty, deg_freedom)[1]
    bound = t_value*std_error
    return bound

class LinearRegression:
    def __init__(self, xdata, ydata, yerror, tags=['x','y']):
        self.x = xdata
        self.y = ydata
        self.n = xdata.count()
        self.yerror = yerror
        self.xtag = tags[0]
        self.ytag = tags[1]

    def scatter_plot(self, name, fit=True):
        fig, ax = plt.subplots(1)

        ax.grid(visible=True, which='major', color='#666666', linestyle='--',
                alpha=0.3)
        ax.set(xlabel=r'{}'.format(self.xtag), ylabel=r'{}'.format(self.ytag))
        ax.errorbar(self.x, self.y, yerr=self.yerror, fmt='ok', markersize=1,
                    capsize=2)

        if not fit:
            plt.show()
        elif fit:
            optimal_params, cov_matrix = optimize.curve_fit(
                # No intercept linear regression
                lambda m,i : m*i, self.x, self.y, sigma=self.yerror)

            slope = optimal_params
            delta_slope = np.sqrt(np.diag(cov_matrix))
            predicted_y = slope*self.x

            #st_dev_mean = stdev_mean(self.x, self.y, slope, self.n)

            confidence_interval_bound = confidence_interval(.05, self.n-1, slope,
                                                            delta_slope)

            ax.set(title=r'\shortstack{{$\what{{\beta}} = {}$, $\what{{\sigma}} = {}$\\ $M = {}$}}'.format(
                '%.4f' % slope, '%.4f' % delta_slope, '%.4f' % confidence_interval_bound))
            ax.plot(self.x, predicted_y, 'k-')
            tikzplotlib.save(f'../pgfplots/{name}'\
                             '.tex', axis_height='6.5cm', axis_width='6.5cm')

            summary = {'Slope': slope, 'Standard error': delta_slope, 'Confidence interval bound':
                       confidence_interval_bound}
            print(summary)
